<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Debugbar;

class AuthController extends Controller
{

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::check()) {
            return redirect('admin/blog');
        }
        else {
            return view('admin.pages.login');
        }
    }
    public function getData() {
        $data = User::paginate(10);
        return view('pages.list', ['data'=>$data]);
    }

    public function login (Request $req) {
        // dd(env('DB_DATABASE'));
        $username = $req->username;
        $password = $req->password;
        $completed = "Login completed!";
        $fail = "Login failed!";
        if (Auth::attempt(['name'=>$username, 'password'=>$password])) {
            return redirect('admin/blog');
        }
        else {
            return view('admin.pages.login', ['message'=>$fail]);
        }
    }
    public function logout() {
        Auth::logout();
        return view('admin.pages.login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $message = 'Register completed!';
        $fail = 'Register failed!';
        $exits = User::where('email','=',$req->email)->get();
        foreach($exits as $item){
            if(isset($item->email) || isset($item->name)) {
                return view('pages.register', ['message'=>$fail]);
            }
        }
        $repeatPassword = $req->passwordrepeat;
        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->number_phone = $req->number_phone;
        $user->birth_day = $req->birth_day;
        $user->password = bcrypt($req->password);

        if($repeatPassword == $req->password) {
            $user->save();
            return view('pages.register',['completed'=>$message]);
        }
        else {
            return view('pages.register', ['message'=>$fail]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = User::where('id','=', $id)->get();
        return view('pages.edit', ['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->number_phone = $request->number_phone;
        $user->birth_day = $request->birth_day;
        $user->save();
        // return redirect("/list");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        $data->delete();
        $message = "Delete Completed!";
        return redirect('/list')->with('success','Delete completed!');;
    }

}
