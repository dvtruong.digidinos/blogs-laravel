<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Posts;
use App\User;
use App\Category;
use App\Http\Requests\FileRequest;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Posts::orderBy('created_at', 'desc')->get();
        $auth = User::get();
        $category = Category::get();
        return view('admin.pages.bloglist', ['data'=>$data, 'auth'=>$auth, 'category'=>$category]);
    }

    public function search(Request $request) {
        $auth = User::get();
        $category = Category::get();
        $id =  $request->get('id');
        $title = $request->get('title');
        $users_id = $request->get('users_id');
        $description = $request->get('description');
        $start_at = $request->get('start_at');
        $end_at = $request->get('end_at');
        $status = $request->get('status');
        $category = $request->get('category');

        $posts = Posts::orderBy('id', 'desc');

        if ($id) {
            $posts = $posts->where('id', 'like', '%'. $id .'%');
        }

        if ($title) {
            $posts = $posts->where('title', 'like', '%' . $title . '%');
        }

        if ($users_id) {
            $posts = $posts->where('users_id', 'like', '%' . $users_id. '%');
        }

        if ($description) {
            $posts = $posts->where('description', 'like', '%' . $description. '%');
        }

        if ($start_at && $end_at) {
            $posts = $posts->whereBetween('created_at', array($start_at,$end_at));
        }


        return view('admin.pages.bloglist', ['data' => $posts->get(), 'auth'=> $auth , 'category'=>$category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.createEditBlog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileRequest $request)
    {
        //
        $completed = "Completed!";
        $failed = "Failed!";

        $file= $request->file('filename');
        $file->move('upload', $file->getClientOriginalName());

        $post = new Posts();
        $post->title = $request->title;
        $post->users_id = $request->users_id;
        $post->description = $request->description;
        $post->content = $request->content;
        $post->image = 'upload/' . $file->getClientOriginalName();
        $post->created_at = Carbon::now();
        $post->updated_at = Carbon::now();
        $post->status = true;
        $post->category_id = 1;

        $post->save();
        return redirect('admin/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Posts::where('id','=', $id)->get();
        return view('admin.pages.createEditBlog', ['data'=>$data, 'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file= $request->file('filename');
        $file->move('upload', $file->getClientOriginalName());

        $post = Posts::find($id);
        $post->title = $request->title;
        $post->users_id = $request->users_id;
        $post->image = 'upload/' . $file->getClientOriginalName();
        $post->description = $request->description;
        $post->content = $request->content;
        $post->created_at = Carbon::now();
        $post->updated_at = Carbon::now();

        $post->save();
        return redirect('admin/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Posts::find($id);
        $data->delete();

        return redirect('admin/blog');
    }
}
