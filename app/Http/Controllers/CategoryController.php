<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Carbon\Carbon;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Category::all();
        return view('admin.pages.category', ['data'=>$data]);
    }

    public function search(Request $request) {
        $id = $request->id;
        $name = $request->name;
        $type = $request->type;
        $description = $request->description;

        $categories = Category::orderBy('id', 'desc');

        if ($id) {
            $categories = $categories->where('id', 'like', '%'. $id .'%');
        }

        if ($name) {
            $categories = $categories->where('name', 'like', '%'. $name .'%');
        }

        if ($type) {
            $categories = $categories->where('type', 'like', '%'. $type .'%');
        }

        if ($description) {
            $categories = $categories->where('description', 'like', '%'. $description .'%');
        }

        return view('admin.pages.category', ['data'=>$categories->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return view('admin.pages.createEditCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category =  new Category();
        $category->name =  $request->name;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->created_at = Carbon::now();
        $category->updated_at = Carbon::now();

        $category->save();
        return redirect('admin/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Category::where('id','=', $id)->get();
        return view('admin.pages.createEditCategory', ['data'=>$data, 'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $category = Category::find($id);
        $category->name =  $request->name;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->created_at = Carbon::now();
        $category->updated_at = Carbon::now();

        $category->save();
        return redirect('admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Category::find($id);

        $data->delete();

        return redirect('admin/category');
    }
}
