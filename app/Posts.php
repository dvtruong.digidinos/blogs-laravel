<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //
    protected $table = 'posts';
    public $timestamps = false;

    public function category() {
        return $this->belongsTo('App\Category');
    }
    public function user() {
        return $this->belongsto('App\User', 'users_id');
    }
}
