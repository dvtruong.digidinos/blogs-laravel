<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NewsBlogController@index');
Route::get('detail', function () {
    return view('web.pages.detail');
});
Route::get('detail/{id}', 'NewsBlogController@show');

Route::group(['prefix'=>'admin'], function () {
    Route::get('/', 'AuthController@index');
    Route::get('logout','AuthController@logout');
    Route::post('/login', 'AuthController@login');
    Route::group(['prefix'=>'blog'], function () {
        Route::get('/', 'BlogController@index')->middleware('check.auth');
        Route::get('create', 'BlogController@create')->name('create')->middleware('check.auth');
        Route::post('create', 'BlogController@store')->middleware('check.auth');
        Route::get('edit/{id}', 'BlogController@edit')->middleware('check.auth');
        Route::PATCH('edit/{id}', 'BlogController@update')->middleware('check.auth');
        Route::delete('delete/{id}', 'BlogController@destroy')->middleware('check.auth');
        Route::post('/', 'BlogController@search')->name('search')->middleware('check.auth');
    });

    Route::group(['prefix'=>'category'], function () {
        Route::get('/', 'CategoryController@index')->middleware('check.auth');
        Route::get('create', function () {
            return view('admin.pages.createEditCategory');
        })->middleware('check.auth');
        Route::post('create', 'CategoryController@store')->middleware('check.auth');;
        Route::get('edit/{id}', 'CategoryController@edit')->middleware('check.auth');
        Route::PATCH('edit/{id}', 'CategoryController@update')->middleware('check.auth');
        Route::delete('delete/{id}', 'CategoryController@destroy')->middleware('check.auth');
        Route::post('search', 'CategoryController@search')->middleware('check.auth');
    });
});
