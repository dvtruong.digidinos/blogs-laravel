<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name'=>'admin',
            'first_name'=> 'admin',
            'last_name'=> 'admin',
            'email'=> 'admin@admin.com',
            'password'=>bcrypt('123'),
            'number_phone'=>'021214564',
            'created_at'=>'2019-12-11',
            'intro'=> 'aaaaaaaaaaaaaaaa',
            'description'=>'vvvvvvvvvvvvvvvcccccc'
        ]);
    }
}
