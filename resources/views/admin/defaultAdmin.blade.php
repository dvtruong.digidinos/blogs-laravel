@include('web.layouts.head')
<body>
    <div class="wrapper">
        {{-- header --}}
        <div class="app">
            @if(Auth::check())
                @include('admin.layouts.header')
                @include('admin.layouts.sidebar')
            @endif
                <div class="main main-content">
                    @yield('admin')
                </div>
        </div>
        {{-- footer --}}
    </div>
</body>
