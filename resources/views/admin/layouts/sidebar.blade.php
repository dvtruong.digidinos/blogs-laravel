<aside class="aside is-placed-left is-left-expanded">
  <div class="aside-tools">
    <div class="aside-tools-label">
      <span class="chevron-left">
          Digidinos
      </span>
      <span class="chevron-right"><b style=" margin-left:5px; font-size:33px">D</b></span>
    </div>
    <a href="javascript:void(0);" class="toggle_fixmenu"><b class="is-left-expanded"><i class="fas fa-chevron-left"></i></b></a>
    <span class="is-left-expanded"><a href="javascript:void(0);" class="toggle_fixmenu"><b class="chevron-right"><i class="fas fa-chevron-right"></i></b></a></span>
  </div>
  <div class="menu-container ps">
    <div class="menu is-menu-main">
      <p class="menu-label">Features</p>
      <ul class="menu-list">
        <li class="">
            <a class="{{  Request::is('admin/blog') || Request::is('admin/blog/search')  ? 'is-active' : ''}} router-link-active has-icon" href="/admin/blog" title="Tables"><span class="icon has-update-mark"><i
                class="mdi mdi-table default"></i></span><span class="menu-item-label">Blog</span>
            </a>
        </li>
        <li class="">
            <a class="{{Request::is('admin/category') ||  Request::is('admin/category/search') ? 'is-active router-link-active' : ''}} has-icon" href="/admin/category" title="Forms"><span class="icon"><i
                class="mdi mdi-square-edit-outline default"></i></span><span class="menu-item-label">Category</span>
            </a>

        </li>
        <li class="">
            <a class="{{Request::is('admin/tags') ? 'is-active router-link-active' : ''}}  has-icon" href="/admin/tags" title="Profile"><span class="icon"><i
                class="mdi mdi-account-circle default"></i></span><span class="menu-item-label">Tags</span>
            </a>

        </li>
      </ul>

      <p class="menu-label">About</p>
      <ul class="menu-list">
        <li class="">
            <a href="#" exact-active-class="is-active" class="has-icon" title="About">
                <span class="icon"><i class="mdi mdi-help-circle default"></i></span>
                <span class="menu-item-label">About</span>
            </a>
        </li>
      </ul>
    </div>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
      <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; right: 0px;">
      <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
    </div>
  </div>
  <div class="menu is-menu-bottom">
    <ul class="menu-list">
      <li class="">
          <a exact-active-class="is-active" class="has-icon is-state-info is-hoverable" title="Log out">
             <span><a href="/admin/logout"> <i class="mdi mdi-logout"></i>Log out</a></span>
          </a>
      </li>
    </ul>
  </div>
</aside>
<script src="{{asset('js/jquery.js')}}"></script>
