@extends('admin.defaultAdmin')
@section('admin')
    <div class="wrapper-table">
        <div class="columns is-mobile title-add">
            <div class="column title">
                Blog Management
            </div>
            <div class="column btn-plus">
                <a class="button is-info is-outlined" href="/admin/blog/create">
                    <span>Add</span><i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div>Search</div>
        <div class="form-search">
            <form action="{{route('search')}}" method="POST">
                @method('POST')
                @csrf
                <div class="columns">
                    <div class="column field">
                        <span class="label">ID</span>
                        <input class="input" type="text" name="id"  value="" placeholder="Enter ID">
                    </div>
                    <div class="column field">
                        <span class="label">Title</span>
                        <input class="input" type="text" name="title"  value="" placeholder="Enter Name">
                    </div>
                </div>
                <div class="columns is-mobile">
                     <div class="column field">
                        <span class="label">Image</span>
                        <div class="select">
                            <select name="type" class="select"  value="">
                                <option value=""></option>
                                @if(isset($auth))
                                    @foreach ($auth as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="column field">
                        <span class="label">Description</span>
                        <input class="input " type="text" name="description"  value="" placeholder="Enter Description">
                    </div>
                </div>

                <div class="columns is-mobile">
                     <div class="column field">
                        <span class="label">Start At</span>
                        <input class="input" type="date" name="start_at"  value="" placeholder="Enter Created At">
                    </div>
                    <div class="column field">
                        <span class="label">End At</span>
                        <input class="input" type="date" name="end_at"  value="" placeholder="Enter Updated At">
                    </div>
                </div>
                <div class="field is-grouped">
                    <button type="submit" class="button is-link is-info ">Submit</button>
                </div>
            </form>
        </div>
        <br>
        <div>Table</div>
        <table class="table is-striped is-narrow is-fullwidth">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Authord</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Create At</th>
                    <th>Update At</th>
                    <th>Status</th>
                    <th>Category</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($data))
                    @foreach ($data as $item)
                        <tr>
                            <th>{{ $item->id }}</th>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td> <img src="{{ url($item->image) }}" alt=""></td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>{{ $item->updated_at }}</td>
                            <td>{{ $item->category_id }}</td>
                            <td>{{ $item->status }}</td>
                            <td><a class="button is-warning" href="/admin/blog/edit/{{$item->id}}">Edit</a></td>
                        <td>
                            <form style="border:none" action="/admin/blog/delete/{{$item->id}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="button is-danger">Delete</button>
                            </form>
                        </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

@endsection
