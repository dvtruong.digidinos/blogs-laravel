@extends('admin.defaultAdmin')
@section('admin')
<div class="flex-center position-ref full-height">
    <div class="form-login">
        <form action="/admin/login" method="POST">
            <div class="title">
                Login
            </div>

            <div class="form">
                @csrf
                <label for="uname"><b>Username</b></label>
                <input class="input" type="text" placeholder="Enter Username" name="username" required>

                <label for="psw"><b>Password</b></label>
                <input class="input" type="password" placeholder="Enter Password" name="password" required>
                <div class="button-form">
                    <button class="button is-success" type="submit">Login</button>
                    <button class="button is-danger" type="button" class="btn-danger">Cancel</button>
                </div>
                @if (isset($message))
                <div class="alert alert-danger">
                    <div class="is-danger">
                        <strong style="color:red"  class="is-danger" id="message-error" >{{ $message }}</strong>
                    </div>
                </div>
                @endif
                <div style="text-align:center">
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </div>
                <div class="" style="text-align:center">
                    <span class=""> <a href="">Forgot password?</a></span>
                </div>
            </div>

        </form>
    </div>
</div>
<script src="{{asset('js/jquery.js')}}"></script>
@endsection
