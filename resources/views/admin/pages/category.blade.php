@extends('admin.defaultAdmin')
@section('admin')
    <div class="wrapper-table">
        <div class="columns title-add">
            <div class="column title">
                Category Management
            </div>
            <div class="column btn-plus">
                <a class="button is-info is-outlined" href="/admin/category/create">
                    <span>Add</span><i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div>Search</div>
        <div class="form-search">
            <form action="/admin/category/search" method="POST">
                @method('POST')
                @csrf
                <div class="columns is-mobile">
                    <div class="column field">
                           <span class="label">ID</span>
                           <input class="input" type="text" name="id"  value="" placeholder="Enter Name Category">
                    </div>
                    <div class="column field">
                           <span class="label">Name</span>
                           <input class="input" type="text" name="name"  value="" placeholder="Enter Name Category">
                    </div>
                </div>
                <div class="columns is-moblie">
                    <div class="column field">
                        <label class="label">Category Type</label>
                         <div class="select">
                            <select name="type" class="select"  value="">
                                <option value="">type A</option>
                                <option value="1">type B</option>
                                <option value="2">type B</option>
                            </select>
                        </div>
                    </div>
                    <div class="column field description">
                        <label class="label">Descriptions</label>
                        <input class="input" name="description" placeholder="Enter Descriptions" value="">
                    </div>
                </div>

                <div class="field is-grouped">
                    <button type="submit" class="button is-link is-info is-medium">Submit</button>
                </div>
            </form>
        </div>
        <br>
        {{-- list --}}
        <div>Table</div>
        <div class="category-list">
            <table data-toggle="table" class="table is-striped is-narrow is-fullwidth pricing__table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Descriptions</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th class="btn-edit">Edit</th>
                        <th class="btn-delete">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data))
                        @foreach ($data as $item)
                        <tr>
                            <th>{{$item->id}}</th>
                            <th>{{$item->name}}</th>
                            <td>{{$item->type}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->updated_at}}</td>
                            <td class="btn-edit">
                                <a class="button is-warning " href="/admin/category/edit/{{$item->id}}" type="">Edit</a>
                            </td>
                            <td class="btn-delete">
                                 <form action="/admin/category/delete/{{$item->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                <button type="submit" class="button is-danger">Delete</button>
                            </form>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
