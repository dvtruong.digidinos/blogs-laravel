@extends('admin.defaultAdmin')
@section('admin')
    <div class="form create-category">
        <div class="title">Create Category</div>
        <form action="/admin/category/{{ Request::is('admin/category/create') ? 'create': 'edit' }}/{{isset($data) ? $id : ''}}" method="post">
            @csrf
            @if(isset($data))
                @method('PATCH')
                @foreach ($data as $item)

                <div class="field">
                    <label class="label">Name</label>
                    <div class="control">
                        <input class="input" type="text" name="name"  value="{{$item->name}}" placeholder="Enter Name Category">
                    </div>
                </div>

                 <div class="field">
                    <label class="label">Category Type</label>
                    <div class="control">
                        <div class="select">
                            <select name="type">
                                <option value="0" selected>type A</option>
                                <option value="1" >type B</option>
                                <option value="2" >type C</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Descriptions</label>
                    <div class="control">
                        <textarea class="textarea" name="description" placeholder="Enter Descriptions" value=""></textarea>
                    </div>
                </div>

                <div class="field is-grouped">
                    <button type="submit" class="button is-link is-success is-medium">Submit</button>
                    <a href="/admin/category" class="button is-link is-light is-danger is-medium">Cancel</a>
                </div>
            </div>
            @endforeach
            @else
            <div class="field">
                    <label class="label">Name</label>
                    <div class="control">
                        <input class="input" type="text" name="name"  value="" placeholder="Enter Name Category">
                    </div>
                </div>

                 <div class="field">
                    <label class="label">Category Type</label>
                    <div class="control">
                        <div class="select">
                            <select name="type"  value="">
                                <option value="0">type A</option>
                                <option value="1">type B</option>
                                <option value="2">type B</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Descriptions</label>
                    <div class="control">
                        <textarea class="textarea" name="description" placeholder="Enter Descriptions" value=""></textarea>
                    </div>
                </div>

                <div class="field is-grouped">
                    <button type="submit" class="button is-link is-success is-medium">Submit</button>
                    <button type="button"  class="button is-link is-light is-danger is-medium">Cancel</button>
                </div>
            </div>
            @endif
        </form>
    </div>
@stop
