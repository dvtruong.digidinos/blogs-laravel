@extends('admin.defaultAdmin')
@section('admin')
    <div class="form create-blog">
        <div>
            <b class="title"> Create Blogs</b>
        </div>
        <br>
        <form action="/admin/blog/{{Request::is('admin/blog/create') ? 'create' : 'edit'}}/{{isset($data) ? $id : ''}}" enctype="multipart/form-data" method="POST">
            @csrf
            @if(isset($data))
            @method('PATCH')
             @foreach ($data as $item)
            <div class="field">
                    <label class="label">Title</label>
                    <div class="control">
                    <input class="input" type="text" name="title"  value="{{$item->title}}" placeholder="Enter Name">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Authord</label>
                    <div class="control">
                        <input class="input" type="text" name="users_id"  value="{{$item->users_id}}" placeholder="Enter Name">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Image</label>
                   <div id="file-js-example" class="file has-name">
                        <label class="file-label">
                            <input class="file-input" type="file" name="filename" >
                            <span class="file-cta">
                            <span class="file-icon">
                                <i class="fas fa-upload"></i>
                            </span>
                            <span class="file-label">
                                Choose a file…
                            </span>
                            </span>
                            <span class="file-name">
                                No file uploaded
                            </span>
                        </label>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Descriptions</label>
                    <div class="control">
                        <textarea  class="textarea" name="description" placeholder="Enter Description" value="">{{$item->description}}</textarea>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Contents</label>
                    <div class="control">
                        <input id="editor" class="textarea" name="content" rows="10" column="10" placeholder="Enter Description" value="{{$item->content}}">
                    </div>
                </div>

                <div class="field is-grouped">
                    <button type="submit" class="button is-link is-success is-medium">Submit</button>
                    <a type="button" href="/admin/"  class="button is-link is-light is-danger is-medium">Cancel</a>
                </div>
            </div>
            @endforeach
            @else
            <div class="field">
                    <label class="label">Title</label>
                    <div class="control">
                    <input class="input" type="text" name="title"  value="" placeholder="Enter Title">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Authord</label>
                    <div class="control">
                        <input class="input" type="text" name="users_id"  value="" placeholder="Enter Name">
                    </div>
                </div>
                <div class="field">
                    <label class="label">Image</label>
                   <div id="file-js-example" class="file has-name">
                        <label class="file-label">
                            <input class="file-input" type="file" name="filename" >
                            <span class="file-cta">
                            <span class="file-icon">
                                <i class="fas fa-upload"></i>
                            </span>
                            <span class="file-label">
                                Choose a file…
                            </span>
                            </span>
                            <span class="file-name">
                                No file uploaded
                            </span>
                        </label>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Descriptions</label>
                    <div class="control">
                        <textarea  class="textarea" name="description" placeholder="Enter Description" value=""></textarea>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Contents</label>
                    <div class="control">
                        <textarea id="editor" class="textarea" name="content" rows="10" column="10" placeholder="Enter Description" value=""></textarea>
                    </div>
                </div>

                <div class="field is-grouped">
                    <button type="submit" class="button is-link is-success" value="upload">Submit</button>
                    <a type="button" href="/admin/"  class="button is-link is-light is-danger ">Cancel</a>
                </div>
            </div>
            @endif
        </form>
    </div>
@endsection
