@extends('web.default')
@section('content')
    <div class="column is-three-quarters">
        @if(isset($data))
        @foreach($data as $item)
        {{-- Information --}}
        <div class="information">
            <div class="columns">
                <div class="column is-three-fifths">
                    <a href="#">{{$item->user->name}}</a>
                    @<span>{{$item->user->email}}</span>
                    <div>{{$item->updated_at}}</div>
                </div>
                <div class="column stats">
                    <div class="columns">
                        <div class="column is-one-fifth stats-item">
                            <span class="tag is-rounded"><i class="fas fa-star"></i>100</span>
                        </div>
                        <div class="column is-one-fifth stats-item">
                            <span class="tag is-rounded"><i class="fas fa-pen"></i>20</span>
                        </div>
                        <div class="column is-one-fifth stats-item">
                            <span class="tag is-rounded"><i class="fas fa-user-plus"></i>10</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Title --}}
        <div class="title">{{$item->title}}</div>
        {{-- Tags --}}
        <div class="tags">
            <span class="tag is-rounded">Post</span>
            <span class="tag is-rounded">Column</span>
            <span class="tag is-rounded">Title</span>
            <span class="tag is-rounded">Recommend</span>
            <span class="tag is-rounded">Related</span>
            <span class="tag is-rounded">News</span>
        </div>
        {{-- Content --}}
        <div class="container content-detail">
            <p>
                {{$item->content}}
            </p>
            <img src="{{url($item->image)}}" alt="">
            <br>
        </div>
        @endforeach
        @endif
    </div>
@endsection
