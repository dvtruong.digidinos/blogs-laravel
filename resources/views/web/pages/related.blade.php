
<div class="">
     <div class="card">
        <header class="card-header">
            <p class="card-header-title">
            Related
            </p>
            <a href="#" class="card-header-icon" aria-label="more options">
            </a>
        </header>
        <div class="card-content">
            <div style="text-align:center;">
                <a href="#">Title</a>
            </div>
            <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
                <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                <br>
                <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
            </div>
        </div>
     </div>
</div>
