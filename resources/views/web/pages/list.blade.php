@extends('web.default')
@section('content')
<div class="column is-three-quarters is-mobile">
    <div class="list-content">
        @if(isset($data))
        @foreach ($data as $item)
        <div class="card-item">
            <div class="card">
                <div class="card-content">
                    <div class="columns">
                        <div class="column image">
                            <img src="{{$item->image}}" alt="">
                        </div>
                        <div class="column">
                            <div class="title is-4"><a href="/detail/{{$item->id}}">{{$item->title}}</a></div>
                            <div class="content">
                                <p>
                                    {{ $item->description }}
                                </p>
                            </div>
                            <a href="#">#css</a> <a href="#">#responsive</a>
                        </div>
                    </div>
                    <br>
                    <time datetime="">{{ $item->updated_at }}</time>
                </div>
            </div>
        </div>
         @endforeach
         {{ $data->links()}}
         @endif
    </div>
</div>

@endsection
