@include('web.layouts.head')
<body>
    <div class="wrapper">
        {{-- header --}}
        @include('web.layouts.header')
        <div class="container web-blog">
            <div class="columns is-mobile">
                @yield('content')
                <div class="column sidebar">
                    <div class="sidebar-item">
                        @include('web.pages.recommend')
                    </div>
                    <div class="sidebar-item">
                        @include('web.pages.related')
                    </div>
                </div>
            </div>
        </div>
        {{-- footer --}}
    </div>
    @include('web.layouts.footer')
</body>
